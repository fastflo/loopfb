#!/usr/bin/python

import fcntl
import mmap
from ctypes import *

class loopfb_ctl(object):
    LOOPFB_CTL_NEWFB = 0xc0706c01
    class newfb_request(Structure):
        _fields_ = [
            ("width", c_int32),
            ("height", c_int32),
            ("stride", c_int32),
            ("format", c_char * 32),
            ("dev_id", c_char * 32),
            ("skip_vt_switch", c_int32),
            
            ("dev_name", c_char * 32)
        ]
            
    LOOPFB_CTL_DELFB = 0xc0206c02
    class delfb_request(Structure):
        _fields_ = [
            ("dev_id", c_char * 32),
        ]

    
    def __init__(self, device_fn="/dev/misc/loopfb-control"):
        self.fd = open(device_fn, "rw")

    def newfb(self, dev_id, width, height, format, stride=None, skip_vt_switch=True):
        """
        returns name of new framebuffer-device
        """
        if stride is None:
            if format == "r5g6b5":
                format_size = 2
            else:
                raise Exception("you need to specify the stride for this format %r!" % format)
            stride = width * format_size
        req = self.newfb_request(width, height, stride, format, dev_id)
        if skip_vt_switch:
            req.skip_vt_switch = 1
        else:
            req.skip_vt_switch = 0
        print "req.skip_vt_switch", req.skip_vt_switch
        ret = fcntl.ioctl(self.fd, self.LOOPFB_CTL_NEWFB, req, True)
        if ret != 0:
            raise Exception("ioctl LOOPFB_CTL_NEWFB returned %d!", ret)
        return req.dev_name
    
    def delfb(self, dev_id):
        req = self.delfb_request(dev_id)
        ret = fcntl.ioctl(self.fd, self.LOOPFB_CTL_DELFB, req, True)
        if ret != 0:
            raise Exception("ioctl LOOPFB_CTL_DELFB returned %d!", ret)

    def mmap(self, dev):
        fd = open(dev, "rb")
        mm = mmap.mmap(fd.fileno(), 160*128*2, mmap.MAP_SHARED)
        print mm[:100]
        
if __name__ == "__main__":
    import argparse
    p = argparse.ArgumentParser()
    p.add_argument("--width", "--cols", "-w", type=int, default=160, help="width of framebuffer to allocate")
    p.add_argument("--height", "--rows", type=int, default=128, help="height of framebuffer to allocate")
    p.add_argument("--format", "-f", type=str, default="r5g6b5", help="framebuffer format")
    p.add_argument("--id", "-i", type=str, default="testfb1", help="if of new framebuffer")
    p.add_argument("--no_skip_vt_switch", action="store_true", help="do not set skip_vt_switch")
    p.add_argument("--del", "-d", dest="delete", action="store_true", help="delete framebuffer with given id")
    p.add_argument("--mmap", "-m", type=str, help="try to mmap framebuffer device")
    args = p.parse_args()
    
    ctl = loopfb_ctl()
    if args.delete:
        ctl.delfb(args.id)
    elif args.mmap:
        ctl.mmap(args.mmap)
    else:
        dev_name = ctl.newfb(args.id, args.width, args.height, args.format, skip_vt_switch=not args.no_skip_vt_switch)
        print "got device name %r" % dev_name

"""
use

echo 0 > /sys/class/vtconsole/vtcon1/bind 

to unbind console from this new framebuffer!

"""
    
