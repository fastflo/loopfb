/*
 * off-screen framebuffer device for sending data back to userlevel
 * 
 * Copyright (C) 2018 Florian Schmidt <florian.schmidt@dlr.de>
 *
 * Based on simplefb.c, which was:
 *
 * Copyright (c) 2013, Stephen Warren
 *
 * Based on q40fb.c, which was:
 * Copyright (C) 2001 Richard Zidlicky <rz@linux-m68k.org>
 *
 * Also based on offb.c, which was:
 * Copyright (C) 1997 Geert Uytterhoeven
 * Copyright (C) 1996 Paul Mackerras
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 */

#include <linux/errno.h>
#include <linux/fb.h>
#include <linux/io.h>
#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/clk.h>
#include <linux/clk-provider.h>
#include <linux/of.h>
#include <linux/of_platform.h>
#include <linux/parser.h>
#include <linux/regulator/consumer.h>
#include <linux/miscdevice.h>
#include <linux/uaccess.h>
#include <loopfb.h>

static struct fb_fix_screeninfo loopfb_fix = {
	.id		= "simple",
	.type		= FB_TYPE_PACKED_PIXELS,
	.visual		= FB_VISUAL_TRUECOLOR,
	.accel		= FB_ACCEL_NONE,
};

static struct fb_var_screeninfo loopfb_var = {
	.height		= -1,
	.width		= -1,
	.activate	= FB_ACTIVATE_NOW,
	.vmode		= FB_VMODE_NONINTERLACED,
};

#define PSEUDO_PALETTE_SIZE 16

static int loopfb_setcolreg(u_int regno, u_int red, u_int green, u_int blue,
			      u_int transp, struct fb_info *info)
{
	u32 *pal = info->pseudo_palette;
	u32 cr = red >> (16 - info->var.red.length);
	u32 cg = green >> (16 - info->var.green.length);
	u32 cb = blue >> (16 - info->var.blue.length);
	u32 value;

	if (regno >= PSEUDO_PALETTE_SIZE)
		return -EINVAL;

	value = (cr << info->var.red.offset) |
		(cg << info->var.green.offset) |
		(cb << info->var.blue.offset);
	if (info->var.transp.length > 0) {
		u32 mask = (1 << info->var.transp.length) - 1;
		mask <<= info->var.transp.offset;
		value |= mask;
	}
	pal[regno] = value;

	return 0;
}

static void loopfb_destroy(struct fb_info *info)
{
	if (info->screen_base) {
		kfree(info->screen_base);
		info->screen_base = NULL;
	}
}

static struct fb_ops loopfb_ops = {
	.owner		= THIS_MODULE,
	.fb_destroy	= loopfb_destroy,
	.fb_setcolreg	= loopfb_setcolreg,
	.fb_fillrect	= cfb_fillrect,
	.fb_copyarea	= cfb_copyarea,
	.fb_imageblit	= cfb_imageblit,
};

static struct loopfb_format loopfb_formats[] = LOOPFB_FORMATS;

struct loopfb_params {
	u32 width;
	u32 height;
	u32 stride;
	struct loopfb_format *format;
};

struct loopfb_par {
	u32 palette[PSEUDO_PALETTE_SIZE];
};

struct loopfb_ctl_data {
	spinlock_t lock;
	struct list_head framebuffers;
};

struct loopfb_framebuffer {
	struct list_head list;

	struct loopfb_ctl_newfb_request req;
	
	struct fb_info *info;
};

struct loopfb_ctl_data* loopfb_data = NULL;


static int loopfb_create(struct loopfb_framebuffer* fb)
{
	int i;
	int ret;
	struct loopfb_params params;
	struct loopfb_par *par;
	struct fb_info *info;
	void* kernel_virt;
	
	printk("%s:%d %s()\n", __FILE__, __LINE__, __func__);

	params.width = fb->req.width;
	params.height = fb->req.height;
	params.stride = fb->req.stride;
	
	params.format = NULL;
	for (i = 0; i < ARRAY_SIZE(loopfb_formats); i++) {
		if (strcmp(fb->req.format, loopfb_formats[i].name))
			continue;
		params.format = &loopfb_formats[i];
		break;
	}
	if (!params.format) {
		pr_err("loopfb: Invalid format '%s'\n", fb->req.format);
		return -EINVAL;
	}

	info = fb->info = framebuffer_alloc(sizeof(struct loopfb_par), NULL /* this should be a struct device* */);
	if (!info)
		return -ENOMEM;

	par = info->par;

	info->fix = loopfb_fix;
	info->fix.smem_len = params.stride * params.height;
	kernel_virt = kzalloc(info->fix.smem_len, GFP_KERNEL);
	info->fix.smem_start = virt_to_phys(kernel_virt);
	if(!info->fix.smem_start) {
		ret = -ENOMEM;
		goto error_fb_release;
	}
	info->fix.line_length = params.stride;

	info->var = loopfb_var;
	info->var.xres = params.width;
	info->var.yres = params.height;
	info->var.xres_virtual = params.width;
	info->var.yres_virtual = params.height;
	info->var.bits_per_pixel = params.format->bits_per_pixel;
	info->var.red = params.format->red;
	info->var.green = params.format->green;
	info->var.blue = params.format->blue;
	info->var.transp = params.format->transp;

	info->apertures = alloc_apertures(1);
	if (!info->apertures) {
		ret = -ENOMEM;
		goto error_mem_release;
	}
	info->apertures->ranges[0].base = info->fix.smem_start;
	info->apertures->ranges[0].size = info->fix.smem_len;

	info->fbops = &loopfb_ops;
	info->flags = FBINFO_DEFAULT | FBINFO_MISC_FIRMWARE;
	info->screen_base = kernel_virt;
	info->pseudo_palette = par->palette;

	pr_info("framebuffer at 0x%lx, 0x%x bytes, mapped to 0x%p\n",
		info->fix.smem_start, info->fix.smem_len,
		info->screen_base);
	pr_info("format=%s, mode=%dx%dx%d, linelength=%d\n",
		params.format->name,
		info->var.xres, info->var.yres,
		info->var.bits_per_pixel, info->fix.line_length);

	info->skip_vt_switch = fb->req.skip_vt_switch;
	pr_warn("info->skip_vt_switch: %d\n", info->skip_vt_switch);
	
	ret = register_framebuffer(info);
	if (ret < 0) {
		pr_err("Unable to register loopfb: %d\n", ret);
		goto error_mem_release;
	}

	pr_info("fb%d: loopfb registered!\n", info->node);
	snprintf(fb->req.dev_name, 32, "fb%d", info->node);

	return 0;

error_mem_release:
	kfree(kernel_virt);
error_fb_release:
	framebuffer_release(info);
	return ret;
}

static int loopfb_remove(struct loopfb_framebuffer* fb)
{
	unregister_framebuffer(fb->info);
	framebuffer_release(fb->info);
	fb->info = NULL;
	
	return 0;
}

static struct loopfb_framebuffer* loopfb_get_fb_by_id(const char* dev_id) {
	struct list_head* ptr;
	struct loopfb_framebuffer* fb = NULL;
	unsigned long flags;
	int found = 0;
	
	spin_lock_irqsave(&loopfb_data->lock, flags);			
	list_for_each(ptr, &loopfb_data->framebuffers) {
		fb = list_entry(ptr, struct loopfb_framebuffer, list);
		if(!strcmp(fb->req.dev_id, dev_id)) {
			found = 1;
			break;
		}
	}
	spin_unlock_irqrestore(&loopfb_data->lock, flags);
	
	if(!found)
		return NULL;
	return fb;
}

static long loopfb_ctl_ioctl(struct file* file, unsigned int cmd, unsigned long arg)
{
	// struct tun_file *tfile = file->private_data;
	void __user* argp = (void __user*)arg;
	int ret = 0;

	pr_info("loopfb_ctl_ioctl cmd %#x\n", cmd);

	ret = 0;
	switch (cmd) {
	case LOOPFB_CTL_NEWFB: {
		struct loopfb_ctl_newfb_request req;
		struct loopfb_framebuffer* fb;
			
		pr_info("LOOPFB_CTL_NEWFB\n");
		if (copy_from_user(&req, argp, sizeof(req)))
			return -EFAULT;
		pr_info("new fb with id '%s', %dx%d, stride %d, format '%s'\n",
			req.dev_id,
			req.width, req.height, req.stride,
			req.format);

		// search whether we already have dev_id
		fb = loopfb_get_fb_by_id(req.dev_id);
		if(fb) {
			int differs = memcmp(&fb->req, &req, sizeof(req) - sizeof(req.dev_name) - sizeof(req.skip_vt_switch));
			pr_info("fb with that name already exists!\n");
			// if yes, and it differs, remove it!
			if(differs) {
				unsigned long flags;
				pr_info("existing fb differs. remove it first!\n");
				
				spin_lock_irqsave(&loopfb_data->lock, flags);			
				loopfb_remove(fb);
				list_del(&fb->list);
				spin_unlock_irqrestore(&loopfb_data->lock, flags);
				
				kfree(fb);
				fb = NULL;
			}
			// if yes and it matches, return that dev_name
		} else
			pr_info("fb with that name does not yet exist!\n");

		if(!fb) {
			unsigned long flags;
			
			// otherwise create a new framebuffer:
			pr_info("create new fb!\n");
			
			fb = kzalloc(sizeof(struct loopfb_framebuffer), GFP_KERNEL);
			if (!fb) {
				pr_err("no memory for new framebuffer!\n");
				return -EFAULT;
			}

			memcpy(&fb->req, &req, sizeof(req));

			if(loopfb_create(fb)) {
				pr_err("failed to create new framebuffer!\n");
				kfree(fb);
				return -EFAULT;
			}
			
			spin_lock_irqsave(&loopfb_data->lock, flags);			
			list_add(&fb->list, &loopfb_data->framebuffers);
			spin_unlock_irqrestore(&loopfb_data->lock, flags);
		}
		
		strcpy(req.dev_name, fb->req.dev_name);
		
		if(copy_to_user(argp, &req, sizeof(req))) {
			pr_err("failed to copy all data to user!\n");
			return -EFAULT;
		}

		break;
	}
	case LOOPFB_CTL_DELFB: {
		struct loopfb_ctl_delfb_request req;
		struct loopfb_framebuffer* fb;
			
		pr_info("LOOPFB_CTL_DELFB\n");
		if (copy_from_user(&req, argp, sizeof(req)))
			return -EFAULT;
		pr_info("del fb with id '%s'\n", req.dev_id);

		// search whether we already have dev_id
		fb = loopfb_get_fb_by_id(req.dev_id);
		if(fb) {
			unsigned long flags;
			pr_info("delete fb at %s\n", fb->req.dev_name);
				
			spin_lock_irqsave(&loopfb_data->lock, flags);			
			loopfb_remove(fb);
			list_del(&fb->list);
			spin_unlock_irqrestore(&loopfb_data->lock, flags);
			
			kfree(fb);
		} else
			pr_info("fb does not exist!\n");

		break;
	}
	default:
		ret = -EINVAL;
		break;
	}

	return ret;
}


static int loopfb_ctl_open(struct inode *inode, struct file * file)
{
	// struct tun_file *tfile;

	pr_info("loopfb_ctl_open\n");
/*
	tfile = (struct tun_file *)sk_alloc(net, AF_UNSPEC, GFP_KERNEL,
					    &tun_proto, 0);
	if (!tfile)
		return -ENOMEM;
	RCU_INIT_POINTER(tfile->tun, NULL);
	tfile->flags = 0;
	tfile->ifindex = 0;

	init_waitqueue_head(&tfile->wq.wait);
	RCU_INIT_POINTER(tfile->socket.wq, &tfile->wq);

	tfile->socket.file = file;
	tfile->socket.ops = &tun_socket_ops;

	sock_init_data(&tfile->socket, &tfile->sk);

	tfile->sk.sk_write_space = tun_sock_write_space;
	tfile->sk.sk_sndbuf = INT_MAX;

	file->private_data = tfile;
	INIT_LIST_HEAD(&tfile->next);

	sock_set_flag(&tfile->sk, SOCK_ZEROCOPY);
*/
	return 0;
}

static int loopfb_ctl_close(struct inode *inode, struct file *file)
{
	// struct tun_file *tfile = file->private_data;
	
	pr_info("loopfb_ctl_close\n");

	// tun_detach(tfile, true);

	return 0;
}

static const struct file_operations loopfb_ctl_fops = {
	.owner	= THIS_MODULE,
	.llseek = no_llseek,
	/*
	.read_iter  = loopfb_chr_read_iter,
	.write_iter = loopfb_chr_write_iter,
	.poll	= loopfb_chr_poll,
	*/
	.unlocked_ioctl	= loopfb_ctl_ioctl,
	.open	= loopfb_ctl_open,
	.release = loopfb_ctl_close,
};

static struct miscdevice loopfb_ctl_miscdev = {
	.minor = MISC_DYNAMIC_MINOR,
	.name = "loopfb-control",
	.nodename = "misc/loopfb-control",
	.fops = &loopfb_ctl_fops,
};

static int __init loopfb_init(void)
{
	int ret = 0;
	
	printk("%s:%d %s()\n", __FILE__, __LINE__, __func__);
	// create chardev that has ioctl to control this module from userlevel

	pr_info("LOOPFB_CTL_NEWFB: %#lx\n", LOOPFB_CTL_NEWFB);
	pr_info("LOOPFB_CTL_DELFB: %#lx\n", LOOPFB_CTL_DELFB);

	
	loopfb_data = kzalloc(sizeof(struct loopfb_ctl_data), GFP_KERNEL);
	if (!loopfb_data) {
		ret = -ENOMEM;
		goto err_alloc;
	}

	spin_lock_init(&loopfb_data->lock);	
	INIT_LIST_HEAD(&loopfb_data->framebuffers);	
	
	ret = misc_register(&loopfb_ctl_miscdev);
	if (ret) {
		pr_err("Can't register misc device\n");
		goto err_misc;
	}

	
	return  0;
err_misc:
	kfree(loopfb_data);
err_alloc:
	return ret;
}

static void __exit loopfb_cleanup(void)
{
	unsigned long flags;
	struct list_head* ptr;
	struct loopfb_framebuffer* fb = NULL;
	
	printk("%s:%d %s()\n", __FILE__, __LINE__, __func__);

	spin_lock_irqsave(&loopfb_data->lock, flags);			
	list_for_each(ptr, &loopfb_data->framebuffers) {
		fb = list_entry(ptr, struct loopfb_framebuffer, list);
		if(fb->info)
			loopfb_remove(fb);
		kfree(fb);
	}
	spin_unlock_irqrestore(&loopfb_data->lock, flags);
	
	misc_deregister(&loopfb_ctl_miscdev);

	kfree(loopfb_data);
	/* Clean up all parport stuff */
	/*
	parport_unregister_driver(&pp_driver);
	class_destroy(ppdev_class);
	unregister_chrdev(PP_MAJOR, CHRDEV);
	*/
}


module_init(loopfb_init);
module_exit(loopfb_cleanup);

/*
static const struct of_device_id loopfb_of_match[] = {
	{ .compatible = "simple-framebuffer", },
	{ },
};
MODULE_DEVICE_TABLE(of, loopfb_of_match);

static struct platform_driver loopfb_driver = {
	.driver = {
		.name = "simple-framebuffer",
		.of_match_table = loopfb_of_match,
	},
	.probe = loopfb_probe,
	.remove = loopfb_remove,
};
*/

/*
static int __init loopfb_init(void)
{
	int ret;
	struct device_node *np;

	ret = platform_driver_register(&loopfb_driver);
	if (ret)
		return ret;

	if (IS_ENABLED(CONFIG_OF_ADDRESS) && of_chosen) {
		for_each_child_of_node(of_chosen, np) {
			if (of_device_is_compatible(np, "simple-framebuffer"))
				of_platform_device_create(np, NULL, NULL);
		}
	}

	return 0;
}
fs_initcall(loopfb_init);
*/
// https://lwn.net/Articles/141730/
// module_platform_driver(loopfb_driver);

MODULE_AUTHOR("Florian Schmidt <florian.schmidt@dlr.de>");
MODULE_DESCRIPTION("off-screen framebuffer driver");
MODULE_LICENSE("GPL v2");
