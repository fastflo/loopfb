#!/usr/bin/python

"""
apt-get install python-pygame-sdl2
"""

import sys
import os

# os.setenv("SDL_VIDEODRIVER=fbcon")
from numpy import *

#import pygame_sdl2
#pygame_sdl2.import_as_pygame()

import pygame
import pygame.display
import pygame.time
import pygame.surfarray

def mask(b):
    return 2 ** b - 1

class fb_reader(object):
    def __init__(self, device):
        self.fd = open(device, "rb")
        self.width = 160
        self.height = 128
        self.img = None
        
    def read_raw(self):
        self.fd.seek(0)
        data = self.fd.read()
        #print "got data of len %d" % len(data)
        data = frombuffer(data, dtype=uint16)
        if self.img is None:
            self.img = empty((self.height * self.width, 3), dtype=uint8)
        img = self.img
        r, g, b = 5, 6, 5
        
        img[:, 0] = (data >> (g + b)) / float(mask(r)) * 255
        img[:, 1] = ((data >> b) & mask(g)) / float(mask(g)) * 255
        img[:, 2] = (data & mask(b)) / float(mask(b)) * 255
        return img.reshape((self.height, self.width, 3)).transpose(1, 0, 2)
        
    def show(self):
        pygame.display.init()
        #print pygame.display.Info()
        #print pygame.display.get_driver()
        
        size = width, height = 160, 128
        screen = pygame.display.set_mode(size)

        #self.animate_ball(screen, size)
        self.show_fb_contents(screen, size)
        
    def show_fb_contents(self, screen, size):
        rect = pygame.Rect(0, 0, self.width, self.height)
        stop_reading = False
        while True:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    sys.exit()

            img = self.read_raw()
            pygame.surfarray.blit_array(screen, img)
            pygame.display.flip()

            pygame.time.wait(100)
        
    def animate_ball(self, screen, size):
        width, height = size
        ball = pygame.image.load("ball.png")
        ballrect = ball.get_rect()
        
        speed = [2, 2]
        black = 0, 0, 0

        while True:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    sys.exit()

            ballrect = ballrect.move(*speed)
            if ballrect.left < 0 or ballrect.right > width:
                speed[0] = -speed[0]
            if ballrect.top < 0 or ballrect.bottom > height:
                speed[1] = -speed[1]

            screen.fill(black)
            screen.blit(ball, ballrect)
            pygame.display.flip()

            #pygame.time.wait(1 / 60. * 1000)
        
if __name__ == "__main__":
    import argparse
    p = argparse.ArgumentParser()
    p.add_argument("--device", "-d", type=str, default="/dev/fb0", help="framebuffer device to read from")
    args = p.parse_args()
    
    r = fb_reader(args.device)
    r.show()
