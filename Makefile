KSRC ?= /lib/modules/$(shell uname -r)/build
MODULE = loopfb

all: modules

modules:
	$(MAKE) -C $(KSRC) M=$(CURDIR) modules

clean:
	-rm -rf *.o *.ko *.mod.* modules.order Module.symvers .*.cmd .tmp_versions/

load: modules
	if [ `id -u` != 0 ]; then sudo make load_as_root; else make load_as_root; fi

load_as_root:
	sync
	echo 8 > /proc/sys/kernel/printk
	dmesg -c >/dev/null
	echo "now unload"> /dev/console
	-((lsmod | grep $(MODULE)) && (echo rmmod; rmmod $(MODULE)))
	echo "now load"> /dev/console
	sync
	insmod ./$(MODULE).ko
	sleep 0.5
	dmesg -c
