/*
 * loopfb.h - off-screen framebuffer device for sending data back to userlevel
 *
 * Copyright (C) 2018 Florian Schmidt <florian.schmidt@dlr.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#ifndef LOOPFB_H
#define LOOPFB_H

#include <drm/drm_fourcc.h>
#include <linux/fb.h>
#include <linux/kernel.h>

/* format array, use it to initialize a "struct loopfb_format" array */
#define LOOPFB_FORMATS \
{ \
	{ "r5g6b5", 16, {11, 5}, {5, 6}, {0, 5}, {0, 0}, DRM_FORMAT_RGB565 }, \
	{ "x1r5g5b5", 16, {10, 5}, {5, 5}, {0, 5}, {0, 0}, DRM_FORMAT_XRGB1555 }, \
	{ "a1r5g5b5", 16, {10, 5}, {5, 5}, {0, 5}, {15, 1}, DRM_FORMAT_ARGB1555 }, \
	{ "r8g8b8", 24, {16, 8}, {8, 8}, {0, 8}, {0, 0}, DRM_FORMAT_RGB888 }, \
	{ "x8r8g8b8", 32, {16, 8}, {8, 8}, {0, 8}, {0, 0}, DRM_FORMAT_XRGB8888 }, \
	{ "a8r8g8b8", 32, {16, 8}, {8, 8}, {0, 8}, {24, 8}, DRM_FORMAT_ARGB8888 }, \
	{ "a8b8g8r8", 32, {0, 8}, {8, 8}, {16, 8}, {24, 8}, DRM_FORMAT_ABGR8888 }, \
	{ "x2r10g10b10", 32, {20, 10}, {10, 10}, {0, 10}, {0, 0}, DRM_FORMAT_XRGB2101010 }, \
	{ "a2r10g10b10", 32, {20, 10}, {10, 10}, {0, 10}, {30, 2}, DRM_FORMAT_ARGB2101010 }, \
}

/*
 * Data-Format for Simple-Framebuffers
 * @name: unique 0-terminated name that can be used to identify the mode
 * @red,green,blue: Offsets and sizes of the single RGB parts
 * @transp: Offset and size of the alpha bits. length=0 means no alpha
 * @fourcc: 32bit DRM four-CC code (see drm_fourcc.h)
 */
struct loopfb_format {
	const char *name;
	u32 bits_per_pixel;
	struct fb_bitfield red;
	struct fb_bitfield green;
	struct fb_bitfield blue;
	struct fb_bitfield transp;
	u32 fourcc;
};

/*
 * Simple-Framebuffer description
 * If the arch-boot code creates simple-framebuffers without DT support, it
 * can pass the width, height, stride and format via this platform-data object.
 * The framebuffer location must be given as IORESOURCE_MEM resource.
 * @format must be a format as described in "struct loopfb_format" above.
 */
struct loopfb_platform_data {
	u32 width;
	u32 height;
	u32 stride;
	const char *format;
};

struct loopfb_ctl_newfb_request {
	u32 width;
	u32 height;
	u32 stride;
	char format[32];
	char dev_id[32];
	u32 skip_vt_switch;

	// return values:
	char dev_name[32];
};

struct loopfb_ctl_delfb_request {
	char dev_id[32];
};

#define LOOPFB_CTL_MAGIC             'l'
#define LOOPFB_CTL_NEWFB             _IOWR(LOOPFB_CTL_MAGIC, 1, struct loopfb_ctl_newfb_request)
#define LOOPFB_CTL_DELFB             _IOWR(LOOPFB_CTL_MAGIC, 2, struct loopfb_ctl_delfb_request)

#endif /* LOOPFB_H */
