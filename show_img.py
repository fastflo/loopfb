#!/usr/bin/python

"""
apt-get install python-pygame-sdl2
"""

import sys
import os

from PIL import Image
from numpy import *

def mask(b):
    return 2 ** b - 1

def write_raw(fd, img):
    im = img.reshape((-1, 3))
    r, g, b = 5, 6, 5
    out = zeros((im.shape[0], ), dtype=uint16)
    out[:] = (im[:, 2] / 255.) * mask(b)
    out[:] |= array((im[:, 1] / 255.) * mask(g), dtype=uint16) << b
    out[:] |= array((im[:, 0] / 255.) * mask(r), dtype=uint16) << (g + b)
    
    fd.seek(0)
    fd.write(out.tostring())

def set_image(device, image):
    fd = open(device, "wb")
    width = 160
    height = 128

    img = Image.open(image)

    if img.size != (width, height):
        print "resizing from", img.size
        img = img.resize((width, height), Image.BICUBIC)
    if img.mode != "RGB":
        print "converting from", img.mode
        img = img.convert("RGB")
    write_raw(fd, array(img))
        
if __name__ == "__main__":
    import argparse
    p = argparse.ArgumentParser()
    p.add_argument("--device", "-d", type=str, default="/dev/fb0", help="framebuffer device to write to")
    p.add_argument("image", type=str, help="image file to show")
    args = p.parse_args()
    
    set_image(args.device, args.image)
